
#include <Arduino.h>

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    char state = LOW;
    while(true) {
        digitalWrite(LED_BUILTIN, state);
        state = !state;
        delay(1000);
    }
}
