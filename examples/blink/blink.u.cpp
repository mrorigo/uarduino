
#define UARDUINO_NO_BUILTIN_TIMER0
#define UARDUINO_NO_POWER_MGMT

#include <uArduino.h>

void setup() {
    u_pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    char state = LOW;
    while(true) {
        u_digitalWrite(LED_BUILTIN, state);
        state = !state;
        bwait(5000);
    }
}
