
#undef UARDUINO_MODULE
#include <uArduino.h>

void init() {
    #ifndef UARDUINO_NO_BUILTIN_TIMERS
    _timer0_init();
    #endif
}

void yield() {
    asm volatile("nop");
}

//int main(void) __attribute__ ((noinline));
int main(void) {
    init();
    setup();
    while(1) {
        loop();
    }
}
