#ifndef __uARDUINO_TIMER0_H__
#define __uARDUINO_TIMER0_H__

#include "uArduino_defs.h"

// macros to convert microseconds to and from clockcycles
#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds(a) ( (a) / clockCyclesPerMicrosecond() )
#define microsecondsToClockCycles(a) ( (a) * clockCyclesPerMicrosecond() )

// If we are included as a
#ifndef UARDUINO_MODULE

#ifdef UARDUINO_BUILTIN_TIMER0

// the prescaler is set so that timer0 ticks every 64 clock cycles, and the
// the overflow handler is called every 256 ticks.
#define MICROSECONDS_PER_TIMER0_OVERFLOW (clockCyclesToMicroseconds(64 * 256))

// the whole number of milliseconds per timer0 overflow
#define MILLIS_INC (MICROSECONDS_PER_TIMER0_OVERFLOW / 1000)

// the fractional number of milliseconds per timer0 overflow. we shift right
// by three to fit these numbers into a byte. (for the clock speeds we care
// about - 8 and 16 MHz - this doesn't lose precision.)
#define FRACT_INC ((MICROSECONDS_PER_TIMER0_OVERFLOW % 1000) >> 3)
#define FRACT_MAX (1000 >> 3)


// By using the 'constructor' attribute, _timer0_init will be run before main()
extern void _timer0_init() __attribute__((constructor,used));

extern uint32_t millis();
extern uint32_t micros();
extern void delay(uint32_t ms);
extern void delayMicroseconds(uint32_t us) ;

#endif // UARDUINO_BUILTIN_TIMER0

#else // UARDUINO_MODULE

extern void _timer0_init();

void delayMicroseconds(uint32_t us);
void delay(uint32_t ms);
uint32_t micros();
uint32_t millis();

#endif

#endif // __UARDUINO_TIMER0__H
