#ifndef __ARDUINO_H__
#define __ARDUINO_H__

// Just a compability/proxy-include

#ifndef ARDUINO_MAIN
#define UARDUINO_MODULE
#endif

#include "uArduino.h"
    
#define digitalWrite(x,y) u_digitalWrite(x,y)
#define digitalRead(x,y) u_digitalRead(x,y)
#define pinMode(x,y) u_pinMode(x,y)

    
#endif
