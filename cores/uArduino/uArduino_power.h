
#ifndef __uArduino_POWER_H__
#define __uArduino_POWER_H__

#include <inttypes.h>	
#include <avr/power.h>
#include <avr/sleep.h>


typedef enum _prr_ref_t {
	PRR_REF_SPI = 0,
	PRR_REF_TWI,
	PRR_REF_ADC,
	PRR_REF_TIM0,
	PRR_REF_TIM1,
	PRR_REF_TIM2,
	PRR_REF_USART0,

	PRR_REF_MAX,
} prr_ref_t;


#ifdef UARDUINO_POWER_MGMT

#ifdef __cplusplus__
extern "C" {
#endif

extern void prr_set_default();

extern uint8_t prr_get();
extern void prr_set(uint8_t _prr);

extern void prr_enable_spi();
extern void prr_disable_spi();
extern void prr_enable_twi();
extern void prr_disable_twi();
extern void prr_enable_adc();
extern void prr_disable_adc();
extern void prr_enable_usart0();
extern void prr_disable_usart0();
extern void prr_enable_timer0();
extern void prr_disable_timer0();
extern void prr_enable_timer1();
extern void prr_disable_timer1();
extern void prr_enable_timer2();
extern void prr_disable_timer2();

#ifdef __cplusplus__
}
#endif

#endif

#endif
