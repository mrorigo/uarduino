#ifndef __uARDUINO_DEFS_H__
#define __uARDUINO_DEFS_H__

////
// Optional defines

// Toggles the built-in timer0 support, disables micros/millis/delay/delayMicroseconds. bwait() still available.
#ifndef UARDUINO_NO_BUILTIN_TIMER0
#define UARDUINO_BUILTIN_TIMER0
#endif
#ifndef UARDUINO_NO_POWER_MGMT
#define UARDUINO_POWER_MGMT
#endif

// Basic AVR includes
#include <stddef.h>
#include <avr/io.h>
#include <avr/interrupt.h>

// PIN modes    
#define OUTPUT 1
#define INPUT 0
#define INPUT_PULLUP 2   // Not supported yet

// PIN states
#define HIGH 1
#define LOW 0

// Bit macros. _BV is defined in avr/sfr_defs.h
#ifndef sbi
#define sbi(x,y) (x |= _BV(y))
#define cbi(x,y) (x &= ~_BV(y))
#endif

#endif