#include "uArduino_power.h"


uint8_t _prr_refcnt[7] = {0,0,0,0,0,0,0};
const uint8_t _prr_mask[7] = {
                1<<PRSPI, 
							  1<<PRTWI, 
							  1<<PRADC, 
							  1<<PRTIM0, 
							  1<<PRTIM1, 
							  1<<PRTIM2, 
							  1<<PRUSART0};

#define PRR_ENABLE(x) if(_prr_refcnt[x] == 0) { PRR &= ~_prr_mask[x]; }; _prr_refcnt[x]++;
#define PRR_DISABLE(x) _prr_refcnt[x]--; if(_prr_refcnt[x] == 0) { PRR |= _prr_mask[x]; };

void prr_set_default() 
{
    PRR = (1<<PRTWI) | /* No TWI by default */  
          (1<<PRTIM2) | /* Not using Timer2 */  
          (1<<PRTIM1) | /* Not using Timer1 */  
#ifndef UARDUINO_BUILTIN_TIMER0                 
          (1<<PRTIM0) | /* Not using timer0 */  
#endif                                          
          (1<<PRSPI) | /* SPI off by default */ 
          (1<<PRADC)  | /* ADC disabled by default */   
          (1<<PRUSART0); /* USART0 off*/
}

uint8_t prr_get() {
  return PRR;
}
void prr_set(uint8_t _prr) {
 PRR = _prr;
}

void prr_enable_spi() {
  PRR_ENABLE(PRR_REF_SPI);
}
void prr_disable_spi() {
  PRR_DISABLE(PRR_REF_SPI);
}
void prr_enable_twi() {
  PRR_ENABLE(PRR_REF_TWI);
}
void prr_disable_twi() {
  PRR_DISABLE(PRR_REF_TWI);
}
void prr_enable_adc() {
  PRR_ENABLE(PRR_REF_ADC);
}
void prr_disable_adc() {
  PRR_DISABLE(PRR_REF_ADC);
}
void prr_enable_usart0() {
  PRR_ENABLE(PRR_REF_USART0);
}
void prr_disable_usart0() {
  PRR_DISABLE(PRR_REF_USART0);
}
void prr_enable_timer0() {
  PRR_ENABLE(PRR_REF_TIM0);
}
void prr_disable_timer0() {
  PRR_DISABLE(PRR_REF_TIM0);
}
void prr_enable_timer1() {
  PRR_ENABLE(PRR_REF_TIM1);
}
void prr_disable_timer1() {
  PRR_DISABLE(PRR_REF_TIM1);
}
void prr_enable_timer2() {
  PRR_ENABLE(PRR_REF_TIM2);
}
void prr_disable_timer2() {
  PRR_DISABLE(PRR_REF_TIM2);
}
