#ifndef __UARDUINO_H__
#define __UARDUINO_H__

#include <stddef.h>
#include <avr/io.h>
#include <avr/interrupt.h>

// Optional defines (define before including uarduino.h)

//Disable built-in timer0 support, disables micros(), millis() and delay()
//#define UARDUINO_NO_BUILTIN_TIMERS

#ifdef __cplusplus__
extern "C" {
#endif

// Useful to be able to compile arduino code
typedef bool boolean;
typedef uint8_t byte;
    
// macros to convert microseconds to and from clockcycles
#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds(a) ( (a) / clockCyclesPerMicrosecond() )
#define microsecondsToClockCycles(a) ( (a) * clockCyclesPerMicrosecond() )

// PIN modes    
#define OUTPUT 1
#define INPUT 0
#define INPUT_PULLUP 2   // Not supported yet

// PIN states
#define HIGH 1
#define LOW 0

// Bit macros. _BV is defined in avr/sfr_defs.h
#define sbi(x,y) (x |= _BV(y))
#define cbi(x,y) (x &= ~_BV(y))

// Macros for setting pinMode and toggling state of pins
// Three defines are required for each pin: uDDR_x, uBIT_x and uPORT_x
#define u_pinMode(x, y) (y==OUTPUT ? (uDDR_##x |= (1<<uBIT_##x)) : (uDDR_##x &= ~(1<<uBIT_##x)), y==INPUT_PULLUP ? (uPORT_##x |= (1<<uBIT_##x)) : (true))
#define u_digitalWrite(x, y) (y==OUTPUT ? (uPORT_##x |= (1<<uBIT_##x)) : (uPORT_##x &= ~(1<<uBIT_##x)))
#define u_digitalRead(x) (uPIN_##x & (1<<uBIT_##x)) ? HIGH : LOW

#define noInterrupts cli
#define interrupts sei

// busywait macro. Calls yield() 1*x
#define bwait(x) ({for(uint32_t __bwi=0; __bwi < x; __bwi++) {yield();yield();yield();yield();yield();yield();yield();yield();yield();yield();}})

#ifndef UARDUINO_MODULE // UARDUINO_MAIN was the alternative

#ifndef yield
void yield() {
    asm volatile("nop");
}
#endif

// Built-in timer0 support
#include "uarduino_timer0.h"


// Called first in main() to initialize timer0
extern void init();
extern void setup();
extern void loop();

#else //#ifdef UARDUINO_MODULE

extern void yield(void);

#endif //#ifdef UARDUINO_MAIN

// Built-in timer0 support
#include "uarduino_timer0.h"

#define uPIN_D0        PIND
#define uPORT_D0       PORTD
#define uDDR_D0        DDRD
#define uBIT_D0        PD0

#define uPIN_D1        PIND
#define uPORT_D1       PORTD
#define uDDR_D1        DDRD
#define uBIT_D1        PD1

#define uPIN_D2        PIND
#define uPORT_D2       PORTD
#define uDDR_D2        DDRD
#define uBIT_D2        PD2

#define uPIN_D3        PIND
#define uPORT_D3       PORTD
#define uDDR_D3        DDRD
#define uBIT_D3        PD3

#define uPIN_D4        PIND
#define uPORT_D4       PORTD
#define uDDR_D4        DDRD
#define uBIT_D4        PD4

#define uPIN_D5        PIND
#define uPORT_D5       PORTD
#define uDDR_D5        DDRD
#define uBIT_D5        PD5

#define uPIN_D6        PIND
#define uPORT_D6       PORTD
#define uDDR_D6        DDRD
#define uBIT_D6        PD6

#define uPIN_D7        PIND
#define uPORT_D7       PORTD
#define uDDR_D7        DDRD
#define uBIT_D7        PD7

#define uPIN_D8        PINB
#define uPORT_D8       PORTB
#define uDDR_D8        DDRB
#define uBIT_D8        PB0

#define uPIN_D9        PINB
#define uPORT_D9       PORTB
#define uDDR_D9        DDRB
#define uBIT_D9        PB1

#define uPIN_D10       PINB
#define uPORT_D10      PORTB
#define uDDR_D10       DDRB
#define uBIT_D10       PB2

#define uPIN_D11       PINB
#define uPORT_D11      PORTB
#define uDDR_D11       DDRB
#define uBIT_D11       PB3

#define uPIN_D12       PINB
#define uPORT_D12      PORTB
#define uDDR_D12       DDRB
#define uBIT_D12       PB4

#define uPIN_D13       PINB
#define uPORT_D13      PORTB
#define uDDR_D13       DDRB
#define uBIT_D13       PB5

#define uPIN_A0        PINC
#define uPORT_A0       PORTC
#define uDDR_A0        DDRC
#define uBIT_A0        PC0

#define uPIN_A1        PINC
#define uPORT_A1       PORTC
#define uDDR_A1        DDRC
#define uBIT_A1        PC1

#define uPIN_A2        PINC
#define uPORT_A2       PORTC
#define uDDR_A2        DDRC
#define uBIT_A2        PC2

#define uPIN_A3        PINC
#define uPORT_A3       PORTC
#define uDDR_A3        DDRC
#define uBIT_A3        PC3

#define uPIN_A4        PINC
#define uPORT_A4       PORTC
#define uDDR_A4        DDRC
#define uBIT_A4        PC4

#define uPIN_A5        PINC
#define uPORT_A5       PORTC
#define uDDR_A5        DDRC
#define uBIT_A5        PC5

// I2C pin names
    
#define uPIN_I2C_SDA         A4
#define uBIT_I2C_SDA         PC4
#define uDDR_I2C_SDA         DDRC
#define uPORT_I2C_SDA        PORTC
#define uADC_I2C_SDA         4

#define uPIN_I2C_SCL         A5
#define uBIT_I2C_SCL         PC5
#define uDDR_I2C_SCL         DDRC
#define uPORT_I2C_SCL        PORTC
#define uADC_I2C_SCL         5

// Built in LED

#define uPORT_LED_BUILTIN    uPORT_D13
#define uDDR_LED_BUILTIN     uDDR_D13
#define uBIT_LED_BUILTIN     uBIT_D13

// SPI pin names

#define uPIN_SPI_SS         uPIN_D10
#define uPORT_SPI_SS        uPORT_D10
#define uDDR_SPI_SS         uDDR_D10
#define uBIT_SPI_SS         uBIT_D10

    
#define uPIN_SPI_MOSI      (11)
#define uPORT_SPI_MOSI     PORTB
#define uDDR_SPI_MOSI      DDRB
#define uBIT_SPI_MOSI      PB3

#define uPIN_SPI_MISO      (12)
#define uPORT_SPI_MISO     PORTB
#define uDDR_SPI_MISO      DDRB
#define uBIT_SPI_MISO      PB4

#define uPIN_SPI_SCK       (13)
#define uPORT_SPI_SCK      PORTB
#define uDDR_SPI_SCK       DDRB
#define uBIT_SPI_SCK       PB5
    

#ifdef __cplusplus__
}
#endif

#endif
