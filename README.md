
# uArduino - u-scopic, Arduino-like library for ATmega & ATtiny MCUs

## Overview

Modular and heavily focused on minimizing code size, uArduino is suitable where you need Arduino-like functionality,
but have very limited memory.

uArduino is also faster than the standard Arduino library, for example when reading or writing digital ports.


## Compability & Differences

uArduino is NOT a full replacement for the standard Arduino library, but serves many of the same purposes in a (very) siilar way.

In particular, uArduino currently lacks support for doing the equivalent of `analogRead()`/`analogWrite()`.

### Timing differences

There are huge differences in how uArduino access the pins/ports, using direct mapping instead of lookup-tables, as is the case in the standard Arduino library.
Hence, a lot less cpu cycles will be spent before actually changing pin and port state, which may result in timing differences for your application.
However, this fact is usually just a good thing, saving both power and time.


### Pin mapping

uArduino uses the same naming convention for pins as the standard Arduino library, but define and reference them differently. In uArduino, pins are defined 
with their port, data direction register and bit number directly, eliminating the need for lookup-tables, but also introducing a few limitations.


... MORE TO COME ...


### Pin usage

To not be confused with, and interopeable with, the standard Arduino library, uArduino defines its own u_pinMode, u_digitalRead and u_digitalwrite.

These are defined as macros, and operate directly on the PORT/DDR registers, hence being very short and fast, but also limits their use. You can not,
for instance, use a variable to store a pin number and then pass that variable to `u_digitalWrite()`. In this case, you would have to introduce an 
if/switch statement to write to the correct pin with a macro, such as;

    if(pNr == 13) {
        u_digitalWrite(D13, state);
    } else if(pNr == 11) {
        u_digitalWrite(D11, state);
    }


### Custom pin naming

To be used with `u_pinMode()` and `u_digitalRead()`/`u_digitalWrite()`, a custom uArduino pin name needs to be defined with three statements:

    #define uPORT_CUSTOMPIN    uPORT_D13
    #define uDDR_CUSTOMPIN     uDDR_D13
    #define uBIT_CUSTOMPIN     uBIT_D13

As can be seen, referencing the already defined standard pin definitions makes this quite intuitive.


## Built-in low-level support

### timer0

uArduino defines the standard timer0-based functions `delay()`, `millis()`, `micros()` and `delayMicroseconds()`, unless they have been disabled using the `UARDUINO_NO_BUILTIN_TIMER0`.

### power management

Unless the `UARDUINO_NO_POWER_MGMT` define has been set, reference counting power reduction support is available using several handy functions. These functions keep reference-counts on the different
bits in the `PRR` register, and enables/disables them appropriately. Enabling these methods uses 7 bytes of eeprom.

    prr_enable_adc() / prr_disable_adc();
    prr_enable_twi() / prr_disable_twi();
    prr_enable_spi() / prr_disable_spi();
    prr_enable_usart0() / prr_disable_usart0();
    prr_enable_timer0() / prr_disable_timer0();
    prr_enable_timer1() / prr_disable_timer1();
    prr_enable_timer2() / prr_disable_timer2();

## Tiny results

The standard 'blink' example..

    #include <uArduino.h>
    
    void setup() {
        u_pinMode(LED_BUILTIN, OUTPUT);
    }

    void loop() {
        char state = LOW;
        while(true) {
            u_digitalWrite(LED_BUILTIN, state);
            state = !state;
            delay(1000);
        }
    }

.. compiles to *548* bytes, using 9 bytes of eeprom!

    AVR Memory Usage
    ----------------
    Device: atmega328p

    Program:     548 bytes (1.7% Full)
    (.text + .data + .bootloader)

    Data:          9 bytes (0.4% Full)
    (.data + .bss + .noinit)

The standard Arduino library compiles this to *868* bytes using 9 bytes of eeprom


## Even tinier 

uArduino, being modular, allows one to disable certain built-in support to further reduce code size, if necessary.

Disabling both timer0 and power management support, and slightly modifying the blink example:

    #define UARDUINO_NO_BUILTIN_TIMER0
    #define UARDUINO_NO_POWER_MGMT
    #include <uArduino.h>

    void setup() {
        u_pinMode(LED_BUILTIN, OUTPUT);
    }

    void loop() {
        char state = LOW;
        while(true) {
            u_digitalWrite(LED_BUILTIN, state);
            state = !state;
            bwait(5000);
        }
    }

.. final size becomes *196* bytes, using *NO* eeprom!


    AVR Memory Usage
    ----------------
    Device: atmega328p

    Program:     178 bytes (0.5% Full)
    (.text + .data + .bootloader)

    Data:          0 bytes (0.0% Full)
    (.data + .bss + .noinit)
    
